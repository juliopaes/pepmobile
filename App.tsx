import React from 'react';
import { StatusBar } from 'react-native';

import AppStack from './src/routes/AppStack';
import AppProvider from './src/hooks';
import { ToastProvider } from './src/hooks/useMessage';
import { Provider as ReduxProvider } from 'react-redux';
import store from './src/redux/store';

export default function App() {
  return (
    <AppProvider>
      <ReduxProvider store={store}>
        <ToastProvider>
          <AppStack />
          <StatusBar barStyle="light-content" />
        </ToastProvider>
      </ReduxProvider>
    </AppProvider>
  );
}
