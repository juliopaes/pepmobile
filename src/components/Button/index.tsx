import React from 'react';
import { RectButtonProperties } from 'react-native-gesture-handler';
import { ContainerButton, Container, ButtonText } from './styles';

interface ButtonProps extends RectButtonProperties {
	children: string;
	outline?: boolean;
	color?: string;
	isNotDefaultButton?: boolean;
	transparent?: boolean;
	type?: 'normal' | 'warning'
	width?: number;
}

export const Button: React.FC<ButtonProps> = ({
	children,
	outline,
	color,
	isNotDefaultButton,
	transparent,
	type,
	width,
	...rest
}) => {
	return (
		<ContainerButton width={width} transparent={transparent} outline={outline} color={color} isNotDefaultButton={isNotDefaultButton} type={type}>
			<Container {...rest}>
				<ButtonText outline={outline} type={type}>{children}</ButtonText>
			</Container>
		</ContainerButton>
	);
}