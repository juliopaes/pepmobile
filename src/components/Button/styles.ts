import styled, { css } from 'styled-components/native';
import { BorderlessButton } from 'react-native-gesture-handler';
import { colors } from '../../styles/color';

interface IButtonProps {
	outline?: boolean;
	color?: string;
	isNotDefaultButton?: boolean;
	transparent?: boolean;
	type?: 'normal' | 'warning';
	width?: number;
}

export const ContainerButton = styled.View<IButtonProps>`
  width: ${props => props.width || 100}%;
	
	background: ${colors.primary};
	border-radius: 4px;
	
	${props => !props.isNotDefaultButton && css`
		margin-top: 8px;
		height: 60px;
	`}

	${props => props.outline && css`
		border: 1px solid ${colors.primary};
		background: ${colors.white};

		${props.type === 'warning' && css`
			border: 1px solid ${colors.orange};
		`}
	`}

	${props => props.color && css`
		background: ${props.color};
	`}

	${props => props.transparent && css`
		background: transparent;
	`}
`;

export const Container = styled(BorderlessButton)`
	width: 100%;
	height: 100%;

	justify-content: center;
	align-items: center;
`;

export const ButtonText = styled.Text<IButtonProps>`
	font-family: 'RobotoSlab-Medium';
	color: ${colors.white};
	font-size: 15px;

	${props => props.outline && css`
			color: ${colors.primary};

			${props.type === 'warning' && css`
				color: ${colors.orange};
			`}
	`}
`;