import React, { isValidElement } from 'react';
import { Text } from 'react-native';
import Feather from 'react-native-vector-icons/Feather';
import { colors } from '../../styles/color';

import {
  Container,
  Header,
  TextHeader,
  TextCard
} from './styles';

interface ICardProps {
  size?: 'small' | 'medium' | 'large';
  icon?: string;
  textHeader?: string;
  body?: string | JSX.Element;
  footer?: string | JSX.Element;
}

export const Card: React.FC<ICardProps> = ({ size, icon, textHeader, body, footer }) => {
  return (
    <Container size={size ? size : 'small'}>
      <Header>
        {icon && <Feather name={icon} size={20} color={colors.primary} />}
        {textHeader && <TextHeader>{textHeader}</TextHeader>}
      </Header>
      {body && isValidElement(body) ? body : <TextCard>{body}</TextCard>}
      {footer && isValidElement(footer) ? footer : <TextCard>{footer}</TextCard>}
    </Container>
  )
}