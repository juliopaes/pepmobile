import styled, { css } from 'styled-components/native';
import { colors } from '../../styles/color';

interface IContainerProps {
  size?: 'small' | 'medium' | 'large';
}

export const Container = styled.View<IContainerProps>`
  width: 100%;

  justify-content: space-between;

  margin: 6px 0px;
  padding: 20px;

  background: ${colors.white};

  border-radius: 4px;

  ${props => props.size === 'small' && css`
    height: 100px;
  `}

  ${props => props.size === 'medium' && css`
    height: 130px;
  `}

  ${props => props.size === 'large' && css`
    height: 160px;
  `}
`;

export const Header = styled.View`
  flex-direction: row;
  align-items: center;
`;

export const TextHeader = styled.Text`
  font-family: 'RobotoSlab-Medium';
  font-size: 14px;

  color: ${colors.primary};

  margin-left: 10px;
`;

export const TextCard = styled.Text`
  font-family: 'RobotoSlab-Regular';
  font-size: 13px;

  color: ${colors.gray};
`;