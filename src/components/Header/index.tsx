import React from 'react';
import { useNavigation } from '@react-navigation/core';
import { DrawerContentComponentProps } from '@react-navigation/drawer';
import {
  TouchableOpacity
} from 'react-native-gesture-handler';

import Feather from 'react-native-vector-icons/Feather';
import logoAsq from '../../assets/images/logo.png';

import {
  Container,
  ContainerHeader,
  Avatar,
  TextHeaderUser,
  LogoAsq
} from './styles';
import { colors } from '../../styles/color';

interface IHeaderProps extends DrawerContentComponentProps {
  textHeader?: string;
  isGoBack?: boolean;
}

export const Header: React.FC<IHeaderProps> = ({ textHeader, isGoBack, ...props }) => {
  const { goBack } = useNavigation();

  return (
    <Container>
      <ContainerHeader>
        <TouchableOpacity onPress={() => { isGoBack ? goBack() : props.navigation.openDrawer(); }}>
          {isGoBack
            ? <Feather name='chevron-left' size={24} color={colors.white} />
            : <Feather name='align-justify' size={34} color={colors.white} />
          }
        </TouchableOpacity>

        <TextHeaderUser>{textHeader}</TextHeaderUser>
      </ContainerHeader>

      {/* <LogoAsq source={logoAsq} resizeMode="contain" /> */}
    </Container>
  )
}