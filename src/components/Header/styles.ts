import styled from 'styled-components/native';
import { colors } from '../../styles/color';

export const Container = styled.View`
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
    padding: 5px 0px;
`;

export const ContainerHeader = styled.View`
    flex-direction: row;
    align-items: center;
`;

export const Avatar = styled.Image`
    width: 40px;
    height: 40px;
    border-radius: 20px;
`;

export const TextHeaderUser = styled.Text`
    font-family: 'RobotoSlab-Medium';
    font-size: 18px;
    margin-left: 10px;
    color: ${colors.white};
`;

export const LogoAsq = styled.Image`
    width: 40px;
    height: 40px;
`;