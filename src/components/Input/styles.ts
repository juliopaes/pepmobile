import styled, { css } from 'styled-components/native';
import { TextInputMask } from 'react-native-masked-text';
import FeatherIcon from 'react-native-vector-icons/Feather';
import { colors } from '../../styles/color';

interface ContainerProps {
    isFocused: boolean;
    isErrored: boolean;
}

export const Container = styled.View<ContainerProps>`
    width: 100%;
    height: 60px;
    padding: 0 16px;
    background: ${colors.white};
    border-radius: 10px;
    margin-bottom: 8px;

    border-width: 2px;
    border-color: ${colors.white};

    flex-direction: row;
    align-items: center;

    ${(props) => props.isErrored && css`
        border-color: ${colors.error.primary};
    `}

    ${(props) => props.isFocused && css`
        border-color: ${colors.secondary};
    `}
`;

export const TextInput = styled.TextInput`
    flex: 1;
    color: ${colors.primary};
    font-size: 16px;
    font-family: 'RobotoSlab-Regular';
`;

export const StyledTextInputMask = styled(TextInputMask)`
    flex: 1;
    color: ${colors.primary};
    font-size: 16px;
    font-family: 'RobotoSlab-Regular';
`;

export const Icon = styled(FeatherIcon)`
    margin-right: 16px;
`;

export const Title = styled.Text`
    font-family: 'RobotoSlab-Medium';
    font-size: 12px;
    color: ${colors.white};
    margin-bottom: 5px;
`;