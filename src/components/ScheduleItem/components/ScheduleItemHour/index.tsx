import React, { memo, useCallback } from 'react';
import { Dispatch } from 'redux';
import { useDispatch } from 'react-redux';
import { format } from 'date-fns'

import {
  Hour,
  HourText
} from './styles';
import { IDispatchProps } from '../../../../redux/types';
import { selectSchedule } from '../../../../redux/actions/Schedule';
import { ptBR } from 'date-fns/locale';

interface ScheduleItemHourProps {
  doctorId: number;
  hour: string;
  hourF: string;
  selected?: boolean;
  disableHourButton?: boolean;
}

const ScheduleItemHour: React.FC<ScheduleItemHourProps> = ({
  hour,
  hourF,
  doctorId,
  selected,
  disableHourButton,
}) => {
  const dispatch = useDispatch<Dispatch<IDispatchProps>>();
  const hourDate = new Date(hour);

  const handleSelectHour = useCallback(() => {
    dispatch(selectSchedule({
      hour,
      doctorId,
      hourF
    }));
  }, [hour, hourF, doctorId]);

  return (
    <Hour
      selected={selected}
      available={true}
      key={hour}
      onPress={!disableHourButton ? handleSelectHour : undefined}
    >
      <HourText>
        {format(hourDate, 'HH:mm', {
          locale: ptBR
        })}
      </HourText>
    </Hour>
  )
}

export default memo(ScheduleItemHour);