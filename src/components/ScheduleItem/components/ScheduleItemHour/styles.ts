import styled from 'styled-components/native';
import { RectButton } from 'react-native-gesture-handler';
import { colors } from '../../../../styles/color';

interface IHoursProps {
  available?: boolean;
  selected?: boolean;
}

interface IHourTextProps {
  selected?: boolean;
}

export const Hour = styled(RectButton) <IHoursProps>`
  width: 55px;
  padding: 10px;
  background: ${props => (props.selected ? colors.secondary : colors.primary)};
  border-radius: 4px;
  
  margin: 4px 8px;

  margin-left: 0px;

  opacity: ${props => (props.available ? 1 : 0.5)};
`;

export const HourText = styled.Text<IHourTextProps>`
  font-family: 'RobotoSlab-Regular';
  font-size: 14px;

  color: ${props => (props.selected ? colors.primary : colors.white)};
`;