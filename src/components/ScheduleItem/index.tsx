import React, { memo, useCallback } from 'react';
import { useNavigation } from '@react-navigation/native';
import { useDispatch } from 'react-redux';
import { Dispatch } from 'redux';
import { IDispatchProps } from '../../redux/types';
import Feather from 'react-native-vector-icons/Feather';
import ScheduleItemHour from './components/ScheduleItemHour';
import { Button } from '../../components/Button';
import { useSelector } from 'react-redux';
import { useMessage } from '../../hooks/useMessage';
import { colors } from '../../styles/color';
import { IDisponibilityResponse } from '../../pages/Schedule'
import { clearSchedule } from '../../redux/actions/Schedule';
import { format } from 'date-fns'

import {
  DoctorContainer,
  DoctorInfo,
  DoctorMeta,
  DoctorMetaText,
  DoctorName,
  ContainerHour,
} from './styles';
import { api } from '../../services/api';
import { useAuth } from '../../hooks/auth';

interface ScheduleItemProps {
  doctor: IDisponibilityResponse;
  isConfirm?: boolean;
  isSchedule?: boolean;
  disableHourButton?: boolean;
}

type ScheduleProps = {
  schedule: string;
}

const ScheduleItem: React.FC<ScheduleItemProps> = ({
  doctor,
  isConfirm,
  disableHourButton,
}) => {
  const { navigate } = useNavigation();
  const { addToast } = useMessage();
  const dispatch = useDispatch<Dispatch<IDispatchProps>>();
  const schedule = useSelector<ScheduleProps, any>(state => state.schedule);
  const { user } = useAuth();

  const navigateToConfirmSchedule = useCallback(async () => {
    if (!isConfirm) {
      navigate('ConfirmSchedule', { doctor });

      return;
    }

    try {
      await api.post('schedule', {
        doctorId: schedule.doctorId,
        finalDate: format(new Date(schedule.hourF), 'yyyy-MM-dd HH:mm'),
        initialDate: format(new Date(schedule.hour), 'yyyy-MM-dd HH:mm'),
        mpi: user.mpi,
        typeService: schedule.type
      });

      dispatch(clearSchedule())

      addToast({
        message: 'Agendamento confirmado com sucesso!',
        type: 'success'
      });

      navigate('Home');
    } catch (e) {
      addToast({
        message: e.response?.data?.message || e.message,
        type: 'error'
      });
    }
  }, [navigate, isConfirm, doctor, schedule, user.mpi]);

  return (
    <DoctorContainer>
      <DoctorInfo>
        <DoctorName>{doctor.nome_completo}</DoctorName>
        <DoctorMeta>
          <Feather name='activity' size={14} color={colors.primary} />
          <DoctorMetaText>{doctor.conselho_profissional}</DoctorMetaText>
        </DoctorMeta>
        <DoctorMeta>
          <Feather name='heart' size={14} color={colors.primary} />
          <DoctorMetaText>{doctor.numero_conselho}</DoctorMetaText>
        </DoctorMeta>
        <DoctorMeta>
          <Feather name='calendar' size={14} color={colors.primary} />
          <DoctorMetaText>Online</DoctorMetaText>
        </DoctorMeta>
        <DoctorMeta>
          <Feather name='clock' size={14} color={colors.primary} />
          <ContainerHour>
            {doctor.disponibilidades.map((hour, index) => (
              <ScheduleItemHour
                key={`${doctor.id}${hour.start}${index}`}
                hour={hour.start}
                hourF={hour.end}
                doctorId={doctor.id}
                selected={schedule.hour === hour.start && schedule.doctorId === doctor.id}
                disableHourButton={disableHourButton}
              />)
            )}
          </ContainerHour>
        </DoctorMeta>
        {
          schedule.doctorId === doctor.id &&
          <Button
            outline
            onPress={navigateToConfirmSchedule}
          >
            Confirmar agendamento
          </Button>
        }
      </DoctorInfo>
    </DoctorContainer>
  )
}

export default memo(ScheduleItem);