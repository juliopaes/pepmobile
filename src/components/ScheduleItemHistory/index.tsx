import React, { memo, useCallback } from 'react';
import { Alert, Linking } from 'react-native'
import Feather from 'react-native-vector-icons/Feather';
import { Button } from '../../components/Button';
import { useMessage } from '../../hooks/useMessage';
import { colors } from '../../styles/color';
import { IScheduleHistoryProps } from '../../pages/ScheduleHistory';
import { api } from '../../services/api';
import { format } from 'date-fns';

import {
  DoctorContainer,
  DoctorInfo,
  DoctorMeta,
  DoctorMetaText,
  DoctorName,
  ContainerHour,
  ContainerButtons,
  ContainerButtonCancel,
  Hour,
  HourText,
} from './styles';

interface ScheduleItemProps {
  scheduleHistory: IScheduleHistoryProps;
  fetchScheduleHistory(): Promise<void>;
}

const ScheduleItemHistory: React.FC<ScheduleItemProps> = ({
  scheduleHistory,
  fetchScheduleHistory,
}) => {
  const { addToast } = useMessage();
  const initialDate = format(new Date(scheduleHistory.data_inicio), 'dd/MM/yyyy HH:mm')

  const handleCancelSchedule = useCallback(async () => {
    async function cancelSchedule() {
      try {
        await api.delete(`/schedule/${scheduleHistory.id}`)

        await fetchScheduleHistory()

        addToast({
          type: 'success',
          message: 'Agendamento cancelado com sucesso!'
        });
      } catch (e) {
        addToast({
          type: 'error',
          message: 'Ocorreu um erro ao cancelar seu agendamento. Tente novamente!'
        });
      }
    }

    Alert.alert('Atenção', 'Confirma o cancelamento da consulta?', [
      {
        text: "Não",
        style: "cancel"
      },
      {
        text: "Sim",
        onPress: () => cancelSchedule()
      }
    ])
  }, [scheduleHistory.id])

  return (
    <DoctorContainer>
      <DoctorInfo>
        <DoctorName>{scheduleHistory.profissional_nome}</DoctorName>
        <DoctorMeta>
          <Feather name='activity' size={14} color={colors.primary} />
          <DoctorMetaText>{scheduleHistory.tipo_servico_descricao}</DoctorMetaText>
        </DoctorMeta>
        <DoctorMeta>
          <Feather name='clock' size={14} color={colors.primary} />
          <ContainerHour>
            <Hour>
              <HourText>
                {initialDate}
              </HourText>
            </Hour>
          </ContainerHour>
        </DoctorMeta>
        <ContainerButtons>
          <Button
            width={50}
            onPress={async () => {
              await Linking.openURL(scheduleHistory.link_paciente);
            }}
          >
            Acessar sala
          </Button>

          <ContainerButtonCancel>
            <Button
              width={50}
              type="warning"
              outline
              onPress={handleCancelSchedule}
            >
              Cancelar
            </Button>
          </ContainerButtonCancel>
        </ContainerButtons>
      </DoctorInfo>
    </DoctorContainer>
  )
}

export default memo(ScheduleItemHistory);