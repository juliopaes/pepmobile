import styled from 'styled-components/native';
import { colors } from '../../styles/color';

export const DoctorContainer = styled.View`
  background: ${colors.white};
  border-radius: 4px;

  padding: 20px;

  margin-bottom: 16px;

  flex-direction: row;
  align-items: center;
`;

export const DoctorInfo = styled.View`
  flex: 1;
`;

export const DoctorName = styled.Text`
  font-family: 'RobotoSlab-Medium';
  font-size: 18px;
  color: ${colors.primary};
`;

export const DoctorMeta = styled.View`
  flex-direction: row;
  align-items: center;
  margin-top: 10px;
`;

export const DoctorMetaText = styled.Text`
  font-family: 'RobotoSlab-Regular';
  
  margin-left: 8px;
  color: ${colors.gray};
`;

export const ContainerHour = styled.View`
  flex-direction: row;
  flex-wrap: wrap;
  margin-left: 10px;
`;

export const ContainerButtons = styled.View`
  flex-direction: row;

  width: 100%;
`;

export const ContainerButtonCancel = styled.View`
  margin-left: 10px;
  width: 100%;
`;

export const Hour = styled.View`
  padding: 10px;
  background: ${colors.primary};
  border-radius: 4px;
  
  margin: 4px 8px;

  margin-left: 0px;
`;

export const HourText = styled.Text`
  font-family: 'RobotoSlab-Regular';
  font-size: 14px;

  color: ${colors.white};
`;