import React, { useEffect, useRef } from "react";
import { Animated } from 'react-native'

import Feather from 'react-native-vector-icons/Feather';
import { useMessage } from "../../hooks/useMessage";

import { colors } from "../../styles/color";

import { Container, ContainerMessage, Message } from "./styles";

interface IToastMessageProps {
  message: string;
  type?: 'info' | 'error' | 'success'
}

const icons = {
  info: <Feather name="info" size={24} color={colors.messages.info} />,
  error: <Feather name="alert-circle" size={24} color={colors.messages.error} />,
  success: <Feather name="check-circle" size={24} color={colors.messages.success} />
}

const ToastMessage: React.FC<IToastMessageProps> = ({ message, type = 'info' }) => {
  const opacity = useRef(new Animated.Value(0)).current;
  const { removeToast } = useMessage();

  useEffect(() => {
    Animated.sequence([
      Animated.timing(opacity, {
        toValue: 1,
        duration: 150,
        useNativeDriver: true
      }),
      Animated.delay(3000),
      Animated.timing(opacity, {
        toValue: 0,
        duration: 500,
        useNativeDriver: true
      })
    ]).start(({ finished }) => {
      finished && removeToast();
    });
  }, []);

  return (
    <Container>
      <ContainerMessage type={type} style={{
        opacity,
        transform: [
          {
            translateY: opacity.interpolate({
              inputRange: [0, 1],
              outputRange: [-20, 0]
            }),
          }
        ],
        elevation: 6,
        shadowColor: colors.gray,
        shadowOffset: {
          width: 0,
          height: 3
        },
        shadowOpacity: 0.15,
        shadowRadius: 5
      }}>
        {icons[type]}
        <Message type={type}>{message}</Message>
      </ContainerMessage>
    </Container>
  );
}

export default ToastMessage;