import styled from 'styled-components/native';
import { Animated } from 'react-native';
import { colors } from '../../styles/color';

interface IContainerMessageProps {
  type: 'info' | 'error' | 'success';
}

interface IMessageProps {
  type: 'info' | 'error' | 'success';
}

export const Container = styled.View`
  position: absolute;
  top: 15px;
  left: 0px;
  right: 0px;
`;

export const ContainerMessage = styled(Animated.View) <IContainerMessageProps>`
  flex-direction: row;

  align-items: center;
  
  margin: 10px;
  margin-bottom: 5px;
  padding: 10px;

  background-color: ${props => colors.messages.background[props.type]};

  border-radius: 5px;
`;

export const Message = styled.Text<IMessageProps>`
  font-size: 15px;
  font-family: 'RobotoSlab-Medium';
  color: ${props => colors.messages[props.type]};

  margin-left: 10px;
`;
