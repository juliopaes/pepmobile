import React, { createContext, useCallback, useState, useContext, useEffect } from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { api } from '../services/api';

interface SignInCredentials {
	cpf: string;
	password: string;
}

interface User {
	id: string;
	nome: string;
	cpf: string;
	mpi: string;
}

interface AuthContextState {
	user: User;
	loadingApp: boolean;
	signIn(credentials: SignInCredentials): Promise<void>;
	signOut(): Promise<void>;
}

interface IAuthResponse {
	data: {
		id: string;
		mpi: string;
		nome: string;
		pepapi_token: string;
	}
}

interface AuthState {
	token: string;
	user: User;
}

const AuthContext = createContext<AuthContextState>({} as AuthContextState);

export function useAuth(): AuthContextState {
	const context = useContext(AuthContext);

	if (!context) {
		throw new Error('useAuth must be used within an AuthProvider');
	}

	return context;
}

export const AuthProvider: React.FC = ({ children }) => {
	const [data, setData] = useState<AuthState>({} as AuthState);
	const [loadingApp, setLoadingApp] = useState(true);

	useEffect(() => {
		async function loadStorageData(): Promise<void> {
			const [token, user] = await AsyncStorage.multiGet([
				'@PepApp:token',
				'@PepApp:user'
			]);

			if (token[1] && user[1]) {
				setData({ token: token[1], user: JSON.parse(user[1]) });
				api.defaults.headers['Authorization'] = `Bearer ${token[1]}`;
			}

			setLoadingApp(false);
		}

		loadStorageData();
	}, []);

	const signIn = useCallback(async ({ cpf, password }) => {
		const response = await api.post<IAuthResponse>('/user/auth', {
			cpf,
			password
		});

		const { data } = response.data;

		// const data = {
		// 	pepapi_token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJBUFAiLCJkYXRhIjp7InVzZXJfaWQiOjI2MywicHJvZmlzc2lvbmFsX2lkIjowLCJiZW5lZmljaWFyaW9faWQiOjAsInJvbGVzIjpbImJlbmVmaWNpYXJpbyJdLCJzaXN0ZW1hX29yaWdlbSI6IkFQUCJ9LCJleHAiOjE2MjQ1ODkxNjMsImlhdCI6MTYyNDU2MDM2M30.8r01VjCu2He3lguezEC-Dcrjbjbw58fEdszLtgMfFK8',
		// 	id: '123',
		// 	nome: 'Júlio Cesar'
		// };

		await AsyncStorage.multiSet([
			['@PepApp:token', data.pepapi_token],
			['@PepApp:user', JSON.stringify(data)]
		]);

		api.defaults.headers['Authorization'] = `Bearer ${data.pepapi_token}`;

		setData({
			token: data.pepapi_token, user: {
				cpf,
				id: data.id,
				nome: data.nome,
				mpi: data.mpi
			}
		});

		setLoadingApp(false);
	}, []);

	const signOut = useCallback(async () => {
		await AsyncStorage.multiRemove(['@PepApp:token', '@PepApp:user']);

		setData({} as AuthState);
	}, []);

	return (
		<AuthContext.Provider value={{
			user: data.user,
			loadingApp,
			signIn,
			signOut
		}}>
			{children}
		</AuthContext.Provider>
	)
}