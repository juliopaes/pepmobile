import React, {
    createContext,
    useCallback,
    useState,
    useContext,
    useEffect
} from 'react';

export interface Doctor {
    id: string;
    name: string;
}

interface DoctorContextState {
    doctor: Doctor;
    setDoctor(doctor: Doctor): Promise<void>;
    removeDoctor(): Promise<void>;
}

export interface DoctorState {
    doctor: Doctor;
}

const AuthContext = createContext<DoctorContextState>({} as DoctorContextState);

export function useDoctor(): DoctorContextState {
    const context = useContext(AuthContext);

    if (!context) {
        throw new Error('useDoctor must be used within an AuthProvider');
    }

    return context;
}

export const DoctorProvider: React.FC = ({ children }) => {
    const [data, setData] = useState<DoctorState>({} as DoctorState);

    useEffect(() => {
        async function loadStorageData(): Promise<void> {

        }

        loadStorageData();
    }, []);

    const handleSetDoctor = useCallback(async (doctor: Doctor): Promise<void> => {
        setData({ doctor });
    }, []);

    const handleRemoveDoctor = useCallback(async (): Promise<void> => {
        setData({} as DoctorState);
    }, []);

    return (
        <AuthContext.Provider value={{
            doctor: data.doctor,
            setDoctor: handleSetDoctor,
            removeDoctor: handleRemoveDoctor
        }}>
            {children}
        </AuthContext.Provider>
    )
}