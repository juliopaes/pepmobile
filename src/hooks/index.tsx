import React from 'react';

import { AuthProvider } from './auth';
import { DoctorProvider } from './doctor';

const AppProvider: React.FC = ({ children }) => {
	return (
		<AuthProvider>
			<DoctorProvider>
				{children}
			</DoctorProvider>
		</AuthProvider>
	);
}

export default AppProvider;