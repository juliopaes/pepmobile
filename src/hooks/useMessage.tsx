import React, { createContext, useContext, useCallback, useState } from 'react';
import ToastContainer from '../components/ToastMessage';

export interface ToastMessage {
    type?: 'success' | 'error' | 'info';
    message: string;
}

interface ToastContextData {
    addToast(message: Omit<ToastMessage, 'id'>): void;
    removeToast(): void;
}

const ToastContext = createContext<ToastContextData>({} as ToastContextData);

export const ToastProvider: React.FC = ({ children }) => {
    const [messages, setMessages] = useState<ToastMessage>({} as ToastMessage);

    const addToast = useCallback(({ type, message }: ToastMessage) => {
        const toast = {
            type,
            message,
        }

        setMessages(() => toast);
    }, []);

    const removeToast = useCallback(() => {
        setMessages({} as ToastMessage);
    }, []);

    return (
        <ToastContext.Provider value={{ addToast, removeToast }}>
            {children}
            {messages && messages.message && <ToastContainer message={messages.message} type={messages.type} />}
        </ToastContext.Provider>
    );
}

export function useMessage(): ToastContextData {
    const context = useContext(ToastContext);

    if (!context) {
        throw new Error('useMessage must be used within a ToastProvider');
    }

    return context;
}