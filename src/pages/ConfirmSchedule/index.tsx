import React from 'react';
import { useRoute } from '@react-navigation/core';
import { DrawerContentComponentProps } from '@react-navigation/drawer';
import { Header } from '../../components/Header';
import ScheduleItem from '../../components/ScheduleItem';
import { IDisponibilityResponse } from '../Schedule';
import { Container, ContainerContent } from './styles';

interface IConfirmScheduleProps {
  doctor: IDisponibilityResponse
}

const ConfirmSchedule: React.FC<DrawerContentComponentProps> = ({ ...props }) => {
  const route = useRoute();
  const { doctor } = route.params as IConfirmScheduleProps;

  return (
    <Container>
      <Header
        textHeader="Resumo do agendamento"
        isGoBack
        {...props}
      />

      <ContainerContent>
        <ScheduleItem isConfirm doctor={doctor} />
      </ContainerContent>
    </Container>
  )
}

export default ConfirmSchedule;