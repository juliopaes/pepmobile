import styled from 'styled-components/native';
import { colors } from '../../styles/color';

export const Container = styled.View`
  flex: 1;

  padding: 20px;

  background: ${colors.primary};
`;

export const ContainerContent = styled.View`
  flex: 1;

  align-items: center;
  justify-content: center;
`;