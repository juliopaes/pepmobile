import React, { useCallback, useMemo, useState } from 'react';
import { useRoute } from '@react-navigation/core';
import { DrawerContentComponentProps } from '@react-navigation/drawer';
import { Header } from '../../components/Header';
import { Button } from '../../components/Button';
import { format, getDaysInMonth } from 'date-fns';
import { Picker } from '@react-native-picker/picker';

import {
  Container,
  DoctorContainer,
  DoctorAvatar,
  Content,
  DoctorName,
  Calendar,
  Schedule,
  Section,
  SectionTitle,
  SectionContent,
  Hour,
  HourText,
  DoctorContainerTag,
  Tag,
  TagContainer,
  SectionDay,
  DayButton,
  DayText,
  WeekText,
  ContainerDay,
  ContainerCalendar,
  ContainerContentCalendar
} from './styles';
import { colors } from '../../styles/color';

interface ICreateScheduleProps {
  doctorId: string;
}

const hours = [{
  hour: 5,
  available: false
}, {
  hour: 6,
  available: true
}, {
  hour: 7,
  available: true
}, {
  hour: 8,
  available: true
}, {
  hour: 9,
  available: true
}, {
  hour: 10,
  available: true
}, {
  hour: 11,
  available: true
}, {
  hour: 12,
  available: true
}, {
  hour: 13,
  available: true
}, {
  hour: 14,
  available: true
}, {
  hour: 15,
  available: true
}, {
  hour: 16,
  available: true
}, {
  hour: 17,
  available: true
}, {
  hour: 18,
  available: true
}];

const week = ['DOM', 'SEG', 'TER', 'QUA', 'QUI', 'SEX', 'SÁB'];
const months = [
  'Janeiro',
  'Fevereiro',
  'Março',
  'Abril',
  'Maio',
  'Junho',
  'Julho',
  'Agosto',
  'Setembro',
  'Outubro',
  'Novembro',
  'Dezembro'
];

const CreateSchedule: React.FC<DrawerContentComponentProps> = ({ ...props }) => {
  const [selectedMonth, setSelectedMonth] = useState(new Date().getMonth());
  const [selectedYear, setSelectedYear] = useState(new Date().getFullYear());
  const [selectedDay, setSelectedDay] = useState(0);
  const [selectedHour, setSelectedHour] = useState(0);
  const [years,] = useState(() => {
    const year = [];

    for (let y = new Date().getFullYear(); y <= new Date().getFullYear() + 2; y++) {
      year.push(y);
    }

    return year;
  })

  const getDaysOfMonth = useMemo(() => {
    const daysInMonth = getDaysInMonth(new Date(selectedYear, selectedMonth, 1));
    const days = [];

    for (let day = 1; day <= daysInMonth; day++) {
      days.push({
        day,
        weekDay: week[new Date(selectedYear, selectedMonth, day).getDay()]
      });
    }

    return days;
  }, [selectedMonth, selectedYear]);

  const morningAvailability = useMemo(() => {
    return hours
      .filter(({ hour }) => { return hour < 12 })
      .map(({ hour, available }) => {
        return {
          hour,
          available,
          hourFormmated: format(new Date().setHours(hour), 'HH:00')
        }
      });
  }, [hours]);

  const afternoonAvailability = useMemo(() => {
    return hours
      .filter(({ hour }) => { return hour >= 12 })
      .map(({ hour, available }) => {
        return {
          hour,
          available,
          hourFormmated: format(new Date().setHours(hour), 'HH:00')
        }
      });
  }, [hours]);

  const handleSelectHour = useCallback((hour: number) => {
    setSelectedHour(hour);
  }, []);

  return (
    <Container>
      <Header
        textHeader='Agendamentos'
        isGoBack
        {...props}
      />
      <Content>
        <DoctorContainer>
          <DoctorAvatar source={{ uri: 'https://i.picsum.photos/id/331/200/200.jpg?hmac=otNh1Xx2hk_Tng_SFa60ayddRGORvWnDKJ2wG1l0KIE' }} />

          <DoctorName>Dr(a). Roberta Barros</DoctorName>

          <DoctorContainerTag>
            <TagContainer>
              <Tag>Nutrição</Tag>
            </TagContainer>
            <TagContainer>
              <Tag>Seg à Sex</Tag>
            </TagContainer>
            <TagContainer>
              <Tag>8h às 18h</Tag>
            </TagContainer>
          </DoctorContainerTag>
        </DoctorContainer>

        <Calendar>
          <ContainerCalendar>
            <ContainerContentCalendar>
              <Picker
                dropdownIconColor={colors.primary}
                selectedValue={selectedMonth}
                style={{ width: 140, fontFamily: 'RobotSlab-Medium' }}
                onValueChange={(itemValue) =>
                  setSelectedMonth(itemValue)
                }>
                {months.map((month, index) => {
                  return (
                    <Picker.Item style={{ fontSize: 12 }} key={index} color={colors.primary} label={month} value={index} />
                  )
                })}
              </Picker>
            </ContainerContentCalendar>
            <ContainerContentCalendar>
              <Picker
                dropdownIconColor={colors.primary}
                selectedValue={selectedYear}
                style={{ width: 140 }}
                onValueChange={(itemValue) =>
                  setSelectedYear(itemValue)
                }>
                {years.map((year, index) => {
                  return (
                    <Picker.Item style={{ fontSize: 12 }} key={index} color={colors.primary} label={year.toString()} value={year} />
                  )
                })}
              </Picker>
            </ContainerContentCalendar>
          </ContainerCalendar>

          <SectionDay>
            {getDaysOfMonth.map(dayOfMonth => {
              return (
                <ContainerDay
                  selected={selectedDay === dayOfMonth.day}
                  key={dayOfMonth.day}
                >
                  <DayButton onPress={() => { setSelectedDay(dayOfMonth.day) }}>
                    <WeekText
                      selected={selectedDay === dayOfMonth.day}
                    >
                      {dayOfMonth.weekDay}
                    </WeekText>
                    <DayText
                      selected={selectedDay === dayOfMonth.day}
                    >
                      {dayOfMonth.day}
                    </DayText>
                  </DayButton>
                </ContainerDay>
              )
            })}
          </SectionDay>
        </Calendar>

        <Schedule>
          <Section>
            <SectionTitle>Manhã</SectionTitle>

            <SectionContent>
              {morningAvailability.map(({ hour, hourFormmated, available }) => (
                <Hour
                  enabled={available}
                  selected={selectedHour === hour}
                  available={available}
                  key={hourFormmated}
                  onPress={() => { handleSelectHour(hour) }}
                >
                  <HourText
                    selected={selectedHour === hour}
                  >
                    {hourFormmated}
                  </HourText>
                </Hour>
              ))}
            </SectionContent>
          </Section>
          <Section>
            <SectionTitle>Tarde</SectionTitle>

            <SectionContent>
              {afternoonAvailability.map(({ hour, hourFormmated, available }) => (
                <Hour
                  enabled={available}
                  selected={selectedHour === hour}
                  available={available}
                  key={hourFormmated}
                  onPress={() => { handleSelectHour(hour) }}
                >
                  <HourText
                    selected={selectedHour === hour}
                  >
                    {hourFormmated}
                  </HourText>
                </Hour>
              ))}
            </SectionContent>
          </Section>
        </Schedule>

        <Button outline>Agendar</Button>
      </Content>
    </Container>
  )
}

export default CreateSchedule;