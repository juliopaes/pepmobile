import styled, { css } from 'styled-components/native';
import { RectButton } from 'react-native-gesture-handler';
import { colors } from '../../styles/color';

interface IDoctorContainerProps {
  selected?: boolean;
}

interface IDoctorNameProps {
  selected?: boolean;
}

interface IHoursProps {
  available: boolean;
  selected: boolean;
}

interface IDayProps {
  selected: boolean;
}

interface IHourTextProps {
  selected: boolean;
}

interface IDayTextProps {
  selected: boolean;
}

export const Container = styled.View`
  flex: 1;

  padding: 20px;

  background: ${colors.primary};
`;

export const Header = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding: 5px 0px;
`;

export const BackButton = styled.TouchableOpacity``;

export const HeaderTitle = styled.Text`
  font-family: 'RobotoSlab-Medium';
  font-size: 20px;

  color: ${colors.white};

  margin-left: 16px;
`;

export const LogoAsq = styled.Image`
  width: 40px;
  height: 40px;

  border-radius: 28px;

  margin-left: auto;
`;

export const DoctorContainer = styled(RectButton) <IDoctorContainerProps>`
  align-items: center;
  justify-content: center;
`;

export const DoctorContainerTag = styled.View`
  flex-direction: row;
  align-items: center;

  margin: 10px 0px;
`;

export const TagContainer = styled.View`
  align-items: center;
  justify-content: center;
  
  padding: 10px;
  margin: 0px 5px;
  
  border-radius: 10px;

  background: ${colors.white};
`;

export const Tag = styled.Text`
  font-family: 'RobotoSlab-Medium';
  font-size: 13px;

  color: ${colors.primary};
`;

export const DoctorAvatar = styled.Image`
  width: 80px;
  height: 80px;

  border-radius: 40px;
`;

export const DoctorName = styled.Text<IDoctorNameProps>`
  font-family: 'RobotoSlab-Medium';
  font-size: 14px;

  margin: 10px 0px;

  color: ${colors.white};
`;

export const Content = styled.ScrollView.attrs({
  showsVerticalScrollIndicator: false
})``;

export const Calendar = styled.View``;

export const Title = styled.Text`
  font-family: 'RobotoSlab-Medium';
  font-size: 18px;

  color: ${colors.white};

  margin: 10px 0px;
`;

export const OpenDatePickerButton = styled(RectButton)`
  height: 46px;
  
  align-items: center;
  justify-content: center;

  background: ${colors.white};

  border-radius: 4px;
`;

export const OpenDatePickerButtonText = styled.Text`
  font-family: 'RobotoSlab-Medium';
  font-size: 16px;

  color: ${colors.primary};
`;

export const Schedule = styled.View`
  padding: 10px 0px;
`;

export const Section = styled.View`
  margin-bottom: 0px;
`;

export const SectionTitle = styled.Text`
  font-size: 14px;
  font-family: 'RobotoSlab-Regular';

  color: ${colors.white};

  margin: 10px 0px;
`

export const SectionContent = styled.View`
  flex-direction: row;
  flex-wrap: wrap;

  width: 100%;
`;

export const SectionDay = styled.ScrollView.attrs({
  horizontal: true,
  showsHorizontalScrollIndicator: false,
  marginTop: 5
})``;

export const DayButton = styled(RectButton)`
  flex: 1;

  align-items: center;
  justify-content: space-between;

  padding: 10px;
  
  border-radius: 20px;
`;

export const ContainerCalendar = styled.View`
  flex-direction: row;
  justify-content: space-between;
`;

export const ContainerContentCalendar = styled.View`
  background: ${colors.white};
  margin: 10px 0px;
  border-radius: 10px;
`;

export const ContainerDay = styled.View<IDayProps>`
  width: 60px;
  height: 60px;

  background: ${props => (props.selected ? colors.secondary : colors.white)};
  
  border-radius: 20px;

  margin-right: 8px;
`;

export const DayText = styled.Text<IDayTextProps>`
  font-family: 'RobotoSlab-Regular';
  font-size: 20px;

  color: ${props => (props.selected ? colors.white : colors.primary)};
`;

export const WeekText = styled.Text<IDayTextProps>`
  font-family: 'RobotoSlab-Medium';
  font-size: 11px;

  color: ${props => (props.selected ? colors.white : colors.primary)};
`;

export const Hour = styled(RectButton) <IHoursProps>`
  width: 55px;
  padding: 10px;
  background: ${props => (props.selected ? colors.secondary : colors.white)};
  border-radius: 4px;
  
  margin: 4px 8px;

  margin-left: 0px;

  opacity: ${props => (props.available ? 1 : 0.5)};
`;

export const HourText = styled.Text<IHourTextProps>`
  font-family: 'RobotoSlab-Regular';
  font-size: 14px;

  color: ${props => (props.selected ? colors.white : colors.primary)};
`;