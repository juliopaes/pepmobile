import React from 'react';
import {
	DrawerContentComponentProps,
	DrawerContentScrollView
} from '@react-navigation/drawer';
import { BorderlessButton, TouchableOpacity } from 'react-native-gesture-handler';
import { useAuth } from '../../hooks/auth';
import Feather from 'react-native-vector-icons/Feather';
import avatarImg from '../../assets/images/logo.png'
import {
	Container,
	SignOut,
	ContainerSignOut,
	TextSignOut,
	ContainerDrawer,
	TextMenu,
	Avatar,
	ContainerMenu,
	IconMenu,
	ContainerImage
} from './styles';

const DrawerContent: React.FC<DrawerContentComponentProps> = ({ ...props }) => {
	const { signOut } = useAuth();
	const { navigate } = props.navigation;

	return (
		<Container>
			<DrawerContentScrollView {...props} style={{ marginBottom: 52 }}>
				<ContainerDrawer>
					<ContainerImage>
						<Avatar source={avatarImg} resizeMode="contain" />
					</ContainerImage>
				</ContainerDrawer>

				<ContainerMenu>
					<IconMenu name="home" size={20} />
					<TouchableOpacity onPress={() => { navigate('Home') }}>
						<TextMenu>Home</TextMenu>
					</TouchableOpacity>
				</ContainerMenu>

				<ContainerMenu>
					<IconMenu name="calendar" size={20} />
					<TouchableOpacity onPress={() => { navigate('Schedule') }}>
						<TextMenu>Agendar consultas</TextMenu>
					</TouchableOpacity>
				</ContainerMenu>

				<ContainerMenu>
					<IconMenu name="clock" size={20} />
					<TouchableOpacity onPress={() => { navigate('ScheduleHistory') }}>
						<TextMenu>Histórico</TextMenu>
					</TouchableOpacity>
				</ContainerMenu>

				{/* <ContainerMenu>
					<IconMenu name="heart" size={20} />
					<TouchableOpacity onPress={() => { navigate('Exams') }}>
						<TextMenu>Sua saúde</TextMenu>
					</TouchableOpacity>
				</ContainerMenu>

				<ContainerMenu>
					<IconMenu name="award" size={20} />
					<TouchableOpacity onPress={() => { navigate('Goals') }}>
						<TextMenu>Metas</TextMenu>
					</TouchableOpacity>
				</ContainerMenu> */}

				<ContainerMenu>
					<IconMenu name="settings" size={20} />
					<TouchableOpacity onPress={() => { navigate('Settings') }}>
						<TextMenu>Configurações</TextMenu>
					</TouchableOpacity>
				</ContainerMenu>
			</DrawerContentScrollView>

			<SignOut>
				<BorderlessButton onPress={() => { signOut() }}>
					<ContainerSignOut>
						<Feather name="power" size={20} color="#d1381a" />
						<TextSignOut>Sair</TextSignOut>
					</ContainerSignOut>
				</BorderlessButton>
			</SignOut>
		</Container>
	)
}

export default DrawerContent;