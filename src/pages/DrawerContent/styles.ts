import styled from 'styled-components/native';
import Feather from 'react-native-vector-icons/Feather';
import { colors } from '../../styles/color';

export const Container = styled.View`
    flex: 1;
`;

export const ContainerDrawer = styled.View`
    flex: 1;
    justify-content: center;
    align-items: center;
    padding: 20px 0px;

    border-bottom-color: ${colors.grayLight};
    border-bottom-width: 1px;
`;

export const ContainerMenu = styled.View`
    flex: 1;
    flex-direction: row;
    padding: 15px 30px;
    margin-top: 10px;
`;

export const Avatar = styled.Image`
    width: 70px;
    height: 70px;
`;

export const ContainerImage = styled.View`
    width: 100px;
    height: 100px;

    justify-content: center;
    align-items: center;

    border-radius: 50px;

    background-color: ${colors.primary};
`;

export const SignOut = styled.View`
    width: 100%;
    height: 50px;

    align-items: center;
    justify-content: center;

    border-top-color: ${colors.grayLight};
    border-top-width: 1px;

    position: absolute;
    bottom: 0;
`;

export const ContainerSignOut = styled.View`
    align-items: center;
    justify-content: center;
    flex-direction: row;
`;

export const TextSignOut = styled.Text`
    font-size: 15px;
    font-family: 'NunitoSans-Regular';
    color: ${colors.red};
    align-items: center;
    justify-content: center;
    margin-left: 10px;
`;

export const TextMenu = styled.Text`
    font-family: 'NunitoSans-Regular';
    font-size: 16px;
    color: ${colors.black};
    margin-left: 10px;
`;

export const IconMenu = styled(Feather)`
    margin-right: 15px;
`;