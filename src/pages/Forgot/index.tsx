import React, { useCallback, useRef } from 'react';
import {
  Platform,
  KeyboardAvoidingView,
  ScrollView
} from 'react-native';
import { useNavigation } from '@react-navigation/core';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { FormHandles } from '@unform/core';
import Input from '../../components/Input';

import logoAsq from '../../assets/images/logo.png';

import {
  Container,
  Logo,
  Title,
  StyledForm,
} from './styles';
import { Button } from '../../components/Button';
import { colors } from '../../styles/color';
import Feather from 'react-native-vector-icons/Feather';

export const Forgot: React.FC = () => {
  const formRef = useRef<FormHandles>(null);
  const { goBack } = useNavigation();

  const handleForgotPassword = useCallback(() => {

  }, []);

  return (
    <Container>
      <TouchableOpacity onPress={() => goBack()}>
        <Feather name='chevron-left' size={25} color='#FFF' style={{ marginTop: 25 }} />
      </TouchableOpacity>
      <KeyboardAvoidingView
        style={{ flex: 1 }}
        behavior={Platform.OS === 'ios' ? 'padding' : undefined}
        enabled
      >
        <ScrollView
          keyboardShouldPersistTaps='handled'
          contentContainerStyle={{ flex: 1 }}
        >
          <Logo source={logoAsq} resizeMode="contain" />

          <Title>Informe seu e-mail{'\n'}para recuperar a senha</Title>

          <StyledForm ref={formRef} onSubmit={handleForgotPassword}>
            <Input
              name="email"
              icon="mail"
              autoCorrect={false}
              autoCapitalize="none"
              keyboardType="email-address"
              text="E-MAIL"
              returnKeyType="done"
              onSubmitEditing={() => {
                formRef.current?.submitForm();
              }}
            />

            <Button
              onPress={() => {
                formRef.current?.submitForm();
              }}
              color={colors.secondary}
            >
              Enviar e-mail
            </Button>
          </StyledForm>
        </ScrollView>
      </KeyboardAvoidingView>
    </Container>
  )
}

export default Forgot;