import styled from 'styled-components/native';
import { Form } from '@unform/mobile';
import { colors } from '../../styles/color';

export const Container = styled.View`
  flex: 1;
  background: ${colors.primary};

  padding: 0px 20px;

  justify-content: center;
`;

export const Logo = styled.Image`
  width: 100px;
  height: 100px;

  margin: 15px 0px;
`;

export const Title = styled.Text`
  font-size: 18px;
  font-family: 'RobotoSlab-Medium';

  color: ${colors.white};
`;

export const StyledForm = styled(Form)`
  margin: 30px 0px;
`;

export const ContainerStep = styled.ScrollView``;