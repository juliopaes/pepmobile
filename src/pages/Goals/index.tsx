import React from 'react';
import { Text } from 'react-native';

import { Container } from './styles';

export const Goals: React.FC = () => {
  return (
    <Container>
      <Text>Goals</Text>
    </Container>
  )
}

export default Goals;