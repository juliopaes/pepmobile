import React, { useState } from 'react';
import {
  ScrollView,
  RefreshControl,
  Linking
} from 'react-native';
import {
  useNavigation
} from '@react-navigation/native';
import {
  DrawerContentComponentProps
} from '@react-navigation/drawer';
import {
  useAuth
} from '../../hooks/auth';
import {
  Container,
  ContainerButton,
  ContainerHistoryButton
} from './styles';

import { Card } from '../../components/Card';
import { Button } from '../../components/Button';
import { Header } from '../../components/Header';

export const Home: React.FC<DrawerContentComponentProps> = ({ ...props }) => {
  const { user } = useAuth();
  const { navigate } = useNavigation();
  const [refresh, setRefresh] = useState(false);

  const onRefreshData = () => {
    setRefresh(true);

    setTimeout(() => {
      setRefresh(false);
    }, 1000);
  }

  return (
    <Container>
      <Header
        textHeader={`Olá, ${user.nome.split(' ', 1)}`}
        {...props}
      />

      <ScrollView
        showsVerticalScrollIndicator={false}
        refreshControl={
          <RefreshControl refreshing={refresh} onRefresh={onRefreshData} />
        }
      >
        <Card
          size='large'
          icon='calendar'
          textHeader='Agendamentos'
          body='Você não possui nenhuma consulta agendada. Que tal realizar a sua primeira?'
          footer={
            <ContainerButton>
              <Button
                isNotDefaultButton={true}
                onPress={() => navigate('Schedule')}
              >
                Agendar
              </Button>

              <ContainerHistoryButton>
                <Button
                  outline
                  isNotDefaultButton={true}
                  onPress={() => navigate('ScheduleHistory')}
                >
                  Histórico
                </Button>
              </ContainerHistoryButton>
            </ContainerButton>
          }
        />

        {/* <Card
          size='large'
          icon='calendar'
          textHeader='Agendamentos'
          body='Sua próxima consulta é no dia 07/05/2020 com o(a) Dr(a). Roberta Barros'
          footer={
            <ContainerButton>
              <Button outline isNotDefaultButton={true} type='warning'>Cancelar</Button>
            </ContainerButton>
          }
        /> */}

        {/* <Card
          size='large'
          icon='calendar'
          textHeader='Agendamentos'
          body='Sua última consulta foi no dia 30/04/2020 com o(a) Dr(a). Roberta Barros'
          footer={
            <ContainerButton>
              <Button
                outline
                isNotDefaultButton={true}
                onPress={() => navigate('Schedule')}
              >
                Agendar
              </Button>
            </ContainerButton>
          }
        /> */}

        {/* <Card
          size='large'
          icon='calendar'
          textHeader='Agendamentos'
          body='Você tem uma consulta hoje com o(a) Dr(a). Roberta Barros.'
          footer={`Horário: 17:30H\nLocal: Online`}
        /> */}

        {/* <Card
          icon='heart'
          textHeader='Sua saúde'
          footer='Você não possui nenhum exame solicitado'
        /> */}

        {/* <Card
          size='large'
          icon='heart'
          textHeader='Histórico de saúde'
          body='Acompanhe seu histórico de saúde no registro do prontuário.'
          footer={
            <ContainerButton>
              <Button outline isNotDefaultButton={true}>Visualizar</Button>
            </ContainerButton>
          }
        /> */}

        {/* <Card
          icon='award'
          textHeader='Metas'
          footer='Você não possui metas a serem cumpridas'
        /> */}

        {/* <Card
          size='large'
          icon='award'
          textHeader='Metas'
          body='Você possui metas que foram solicitados na sua última consulta no dia 30/04/2020'
          footer={
            <ContainerButton>
              <Button outline isNotDefaultButton={true}>Ver metas</Button>
            </ContainerButton>
          }
        /> */}

        <Card
          size='large'
          icon='phone'
          textHeader='Atendimento'
          body='Ligue agora para a central de atendimento e converse com uma enfermeira'
          footer={
            <ContainerButton>
              <Button
                outline
                isNotDefaultButton={true}
                onPress={() => {
                  Linking.openURL(`tel:48996351331`)
                }}
              >
                Ligar
              </Button>
            </ContainerButton>
          }
        />
      </ScrollView>
    </Container>
  )
}

export default Home;