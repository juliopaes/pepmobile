import styled from 'styled-components/native'
import { colors } from '../../styles/color';

export const Container = styled.View`
  flex: 1;

  padding: 20px;

  background: ${colors.primary};
`;

export const ContainerButton = styled.View`
    width: 100px;
    max-height: 30px;

    flex-direction: row;
    justify-content: space-between;
`;

export const ContainerHistoryButton = styled.View`
  margin-left: 10px;
  width: 100px;
`;