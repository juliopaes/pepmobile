import React from 'react';
import {
  Image,
  Text,
  View
} from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { TouchableOpacity } from 'react-native-gesture-handler'
import Swiper from 'react-native-swiper'
import { Button } from '../../components/Button';
import logoAsq from '../../assets/images/logo.png';
import doctorImgFirst from '../../assets/images/doctor1.jpg';
import doctorImgSecond from '../../assets/images/doctor2.jpg';

import {
  Container,
  ContainerLogo,
  ContainerButtons,
  Title,
  SubTitle,
  Background,
  TextDoubt
} from './styles';

export const Landing: React.FC = () => {
  const { navigate } = useNavigation();

  return (
    <Container>
      <Swiper autoplay={true} autoplayTimeout={3.0}>
        <Background source={doctorImgFirst} resizeMode="cover" imageStyle={{shadowOpacity: 0.5}}>
          <Title>As melhores soluções de saúde para você que é beneficiário da "Nome operadora"</Title>
        </Background>

        <Background source={doctorImgSecond} resizeMode="cover" imageStyle={{shadowOpacity: 0.5}}>
          <Title>72% dos usuários de Telemedicina não precisam de atendimento presencial. Fale com um especialista sem sair de casa!</Title>
        </Background>
      </Swiper>

      <ContainerButtons>
        <Button outline={true} transparent={true} onPress={() => { navigate('Home') }}>
          Acessar
        </Button>
        <TouchableOpacity onPress={() => { navigate('Home') }}>
          <SubTitle>
            Tem dúvidas? Acesse aqui.
          </SubTitle>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => { navigate('Home') }}>
          <SubTitle>
            Política de privacidade
          </SubTitle>
        </TouchableOpacity>
      </ContainerButtons>
    </Container>
  )


}

export default Landing;