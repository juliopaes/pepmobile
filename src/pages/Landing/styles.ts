import styled from 'styled-components/native';
import { colors } from '../../styles/color';

export const Container = styled.View`
  flex: 1;
  
  justify-content: flex-end;
`;

export const ContainerLogo = styled.View`
  flex: 1;
  
  justify-content: center;

  padding: 0px 20px;
`;

export const Background = styled.ImageBackground`
  flex: 1;

  align-items: center;
  justify-content: center;

  padding: 25px;
`;

export const ContainerButtons = styled.View`
  position: absolute;
  bottom: 10%;
  
  width: 100%;

  align-items: center;
  justify-content: center;

  padding: 0px 30px;
`;

export const Title = styled.Text`
  position: absolute;
  bottom: 50%;
  
  font-size: 18px;
  font-family: 'RobotoSlab-Medium';

  color: ${colors.white};

  margin-top: 40px;
  text-align: center;
`;

export const SubTitle = styled.Text`
  font-size: 14px;
  font-family: 'RobotoSlab-Regular';

  color: ${colors.white};

  margin: 10px 0px;
`;

export const TextDoubt = styled.Text`
  font-size: 13px;
  font-family: 'RobotoSlab-Regular';

  color: ${colors.white};

  margin: 10px 0px;
`;