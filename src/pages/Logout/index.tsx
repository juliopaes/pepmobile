import React from 'react';
import { useEffect } from 'react';
import { ActivityIndicator } from 'react-native'

import { useAuth } from '../../hooks/auth'
import { useMessage } from '../../hooks/useMessage'

import { colors } from '../../styles/color';
import { Container } from './styles'

const Logout: React.FC = () => {
  const { signOut } = useAuth();
  const { addToast } = useMessage();

  useEffect(() => {
    signOut();

    addToast({
      type: 'info',
      message: 'Sua sessão foi encerrada. Faça login novamente!'
    });
  }, []);

  return (
    <Container>
      <ActivityIndicator size="large" color={colors.white} />
    </Container>
  )
}

export default Logout;