import styled from 'styled-components/native'
import { colors } from '../../styles/color'

export const Container = styled.View`
  flex: 1;

  justify-content: center;
  align-items: center;

  padding: 20px;

  background: ${colors.primary};
`