import React, { useCallback, useState, useEffect } from 'react';
import { FlatList, Platform, Modal, ActivityIndicator } from 'react-native';
import { Dispatch } from 'redux';
import { DrawerContentComponentProps } from '@react-navigation/drawer';
import { Header } from '../../components/Header';
import { Button } from '../../components/Button';
import ScheduleItem from '../../components/ScheduleItem';
import { Picker } from '@react-native-picker/picker';
import DateTimePicker from '@react-native-community/datetimepicker';
import { format } from 'date-fns';
import ptBR from 'date-fns/locale/pt-BR';
import { useMessage } from '../../hooks/useMessage'
import { useDispatch } from 'react-redux';
import {
  Container,
  ContainerSpecialtie,
  TextSpecialtie,
  Calendar,
  ContainerPeriod,
  TagContainer,
  Tag,
  ContainerModal,
  EmptyList,
  EmptyText,
} from './styles';
import { colors } from '../../styles/color';
import { api } from '../../services/api';
import { IDispatchProps } from '../../redux/types';
import { clearSchedule, selectType } from '../../redux/actions/Schedule';

export interface IDoctorsListProps {
  id: string;
  avatar: string;
  name: string;
  specialtie: string;
  description: string;
  hours: Array<string>;
}

export interface IDisponibilityHourProps {
  date: string;
  start: string;
  end: string;
}

export interface IDisponibilityResponse {
  id: number;
  nome_completo: string;
  numero_conselho: string;
  conselho_profissional: string;
  uf_conselho: string;
  disponibilidades: Array<IDisponibilityHourProps>;
}

interface ITypeServices {
  id: number;
  descricao: string;
}

export const Schedule: React.FC<DrawerContentComponentProps> = ({ ...props }) => {
  const [date, setDate] = useState<Date>();
  const [showDatePicker, setShowDatePicker] = useState(false);
  const [selectedSpecialtie, setSelectedSpecialtie] = useState<number>();
  const [selectedPeriod, setSelectedPeriod] = useState('manha');
  const [showModal, setShowModal] = useState(false);
  const [typeServices, setTypeServices] = useState<ITypeServices[]>([]);
  const [disponibility, setDisponibility] = useState<IDisponibilityResponse[]>([])
  const { addToast } = useMessage();
  const dispatch = useDispatch<Dispatch<IDispatchProps>>();

  useEffect(() => {
    async function fetchTypeServices() {
      setShowModal(true);

      try {
        const response = await api.get<ITypeServices[]>('/typeservices');

        setTypeServices(response.data);

        setShowModal(false);
      } catch (e) {
        setShowModal(false);

        if (e.response?.status !== 401) {
          addToast({
            type: 'error',
            message: 'Ocorreu um erro ao buscar os tipos de serviço. Tente novamente mais tarde!'
          });
        }
      }
    }

    fetchTypeServices();
  }, []);

  useEffect(() => {
    async function fetchDoctorDisponibility(initialDate: Date) {
      setShowModal(true);
      dispatch(clearSchedule())

      if (selectedSpecialtie) {
        dispatch(selectType(selectedSpecialtie))
      }

      try {
        const response = await api.get<IDisponibilityResponse[]>(`/disponibility`, {
          params: {
            period: selectedPeriod,
            initialDate: format(initialDate, 'yyyy-MM-dd'),
            typeService: selectedSpecialtie
          }
        });

        setShowModal(false);
        setDisponibility(response.data);
      } catch (e) {
        setShowModal(false);
        setDisponibility([]);

        if (e.response?.status !== 401) {
          addToast({
            type: 'error',
            message: e.response?.data?.message || e.message
          });
        }
      }
    }

    if (date) {
      fetchDoctorDisponibility(date)
    }
  }, [date, selectedPeriod, selectedSpecialtie]);

  const handleToggleDatePicker = useCallback(() => {
    setShowDatePicker(state => !state);
  }, []);

  const handleDateChanged = useCallback((event: any, date: Date | undefined) => {
    if (Platform.OS === 'android') {
      setShowDatePicker(false);
    }

    if (date) {
      setDate(date);
    }
  }, []);

  return (
    <>
      <Modal
        animationType="fade"
        visible={showModal}
        transparent={true}
      >
        <ContainerModal>
          <ActivityIndicator size="large" color="#FFF" />
        </ContainerModal>
      </Modal>
      <Container>
        <Header
          textHeader='Agendamentos'
          {...props}
        />

        <TextSpecialtie>Selecione a especialidade</TextSpecialtie>

        <ContainerSpecialtie>
          <Picker
            dropdownIconColor={colors.primary}
            selectedValue={selectedSpecialtie}
            onValueChange={(itemValue: number) => {
              setSelectedSpecialtie(itemValue)
            }
            }>
            {typeServices.map(typeService => {
              return (
                <Picker.Item
                  key={typeService.id}
                  color={colors.primary}
                  fontFamily='RobotSlab-Regular'
                  label={typeService.descricao}
                  value={typeService.id}
                />
              )
            })}
          </Picker>
        </ContainerSpecialtie>

        <Calendar>
          <Button color={colors.secondary} onPress={handleToggleDatePicker}>
            {date ? format(date, "dd 'de' MMMM 'de' yyyy", {
              locale: ptBR
            }) : 'Selecione a data'}
          </Button>

          {showDatePicker &&
            <DateTimePicker
              value={date || new Date()}
              mode="date"
              display="calendar"
              onChange={handleDateChanged}
            />
          }
        </Calendar>

        <ContainerPeriod>
          <TagContainer selected={selectedPeriod === 'manha'} onPress={() => { setSelectedPeriod('manha') }}>
            <Tag selected={selectedPeriod === 'manha'}>Manhã</Tag>
          </TagContainer>
          <TagContainer selected={selectedPeriod === 'tarde'} onPress={() => { setSelectedPeriod('tarde') }}>
            <Tag selected={selectedPeriod === 'tarde'}>Tarde</Tag>
          </TagContainer>
          <TagContainer selected={selectedPeriod === 'noite'} onPress={() => { setSelectedPeriod('noite') }}>
            <Tag selected={selectedPeriod === 'noite'}>Noite</Tag>
          </TagContainer>
        </ContainerPeriod>

        <FlatList<IDisponibilityResponse>
          data={disponibility}
          keyExtractor={(doctor) => doctor.id.toString()}
          contentContainerStyle={{ marginTop: 10, opacity: showModal ? 0.5 : 1 }}
          renderItem={({ item: doctor }) => <ScheduleItem key={doctor.id} doctor={doctor} />}
          ListEmptyComponent={() => <EmptyList><EmptyText>Nenhum médico encontrado para os filtros selecionados</EmptyText></EmptyList>}
        />
      </Container>
    </>
  )
}

export default Schedule;