import styled, { css } from 'styled-components/native'
import { RectButton } from 'react-native-gesture-handler';
import { colors } from '../../styles/color';

interface ITagContainerProps {
  selected?: boolean;
}

interface IContainerProps {
  showOpacity?: boolean;
}

export const Container = styled.View<IContainerProps>`
  flex: 1;

  padding: 20px;

  background: ${colors.primary};
`;

export const ContainerSpecialtie = styled.View`
  background: ${colors.white};

  border-radius: 4px;
`;

export const TextSpecialtie = styled.Text`
  font-family: 'RobotoSlab-Regular';
  font-size: 14px;

  color: ${colors.white};

  margin: 10px 0px;
`;

export const Calendar = styled.View``;

export const ContainerPeriod = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: space-between;

  padding: 10px 0px;
`;

export const TagContainer = styled(RectButton) <ITagContainerProps>`
  align-items: center;
  justify-content: center;
  
  padding: 10px 25px;
  
  border-radius: 5px;

  background: ${colors.white};

  ${props => props.selected &&
    css`
      background: ${colors.secondary};
    `
  }
`;

export const Tag = styled.Text<ITagContainerProps>`
  font-family: 'RobotoSlab-Medium';
  font-size: 13px;

  color: ${colors.primary};

  ${props => props.selected &&
    css`
      color: ${colors.white};
    `
  }
`;

export const ContainerModal = styled.View`
  flex: 1;
  
  justify-content: center;
  align-items: center;
`;

export const EmptyList = styled.View`
  flex: 1;

  height: 200px;

  align-items: center;
  justify-content: center;
`;

export const EmptyText = styled.Text`
  font-family: 'RobotoSlab-Medium';
  font-size: 18px;
  color: ${colors.white};

  text-align: center;
`;