import React, { useEffect, useState, useCallback } from 'react';
import { FlatList, Modal, ActivityIndicator } from 'react-native';
import { useIsFocused } from '@react-navigation/native';
import { DrawerContentComponentProps } from '@react-navigation/drawer';
import { Header } from '../../components/Header';
import ScheduleItemHistory from '../../components/ScheduleItemHistory';
import { format, addDays } from 'date-fns';

import {
  Container,
  ContainerContent,
  NextSchedules,
  EmptyList,
  EmptyText,
  ContainerModal,
} from './styles';
import { api } from '../../services/api';
import { useAuth } from '../../hooks/auth';

export interface IScheduleHistoryProps {
  data_inicio: string;
  id: number,
  profissional_nome: string;
  status_descricao: string;
  tipo_servico_descricao: string;
  link_paciente: string;
}

const ScheduleHistory: React.FC<DrawerContentComponentProps> = ({ ...props }) => {
  const { user } = useAuth();
  const [scheduleHistory, setScheduleHistory] = useState<IScheduleHistoryProps[]>([]);
  const [showModal, setShowModal] = useState(false);
  const isFocused = useIsFocused();

  const fetchScheduleHistory = useCallback(async () => {
    setShowModal(true)

    try {
      const dateI = new Date();
      const dateF = addDays(dateI, 30);

      const response = await api.get<IScheduleHistoryProps[]>('schedulehistory', {
        params: {
          initialDate: format(dateI, 'yyyy-MM-dd'),
          finalDate: format(dateF, 'yyyy-MM-dd'),
          mpi: user.mpi
        }
      });

      const schedules = response.data.filter((schedule) => {
        return schedule.status_descricao !== 'Cancelado'
      })

      setScheduleHistory(schedules);
      setShowModal(false);
    } catch (e) {
      setShowModal(false);
    }
  }, [])

  useEffect(() => {
    fetchScheduleHistory();
  }, [fetchScheduleHistory, isFocused]);

  return (
    <>
      <Modal
        animationType="fade"
        visible={showModal}
        transparent={true}
      >
        <ContainerModal>
          <ActivityIndicator size="large" color="#FFF" />
        </ContainerModal>
      </Modal>
      <Container>
        <Header
          textHeader="Histórico de agendamento"
          isGoBack
          {...props}
        />

        <ContainerContent>
          {scheduleHistory[0] && <ScheduleItemHistory scheduleHistory={scheduleHistory[0]} fetchScheduleHistory={fetchScheduleHistory} />}

          {
            scheduleHistory.length > 0 && <NextSchedules>Suas próximas consultas</NextSchedules>
          }

          <FlatList<IScheduleHistoryProps>
            data={scheduleHistory}
            renderItem={({ item }) => <ScheduleItemHistory key={item.id} scheduleHistory={item} fetchScheduleHistory={fetchScheduleHistory} />}
            keyExtractor={(item) => item.id.toString()}
            onRefresh={() => { }}
            contentContainerStyle={{ flex: scheduleHistory.length ? undefined : 1 }}
            refreshing={false}
            ListEmptyComponent={() => <EmptyList><EmptyText>Você não possúi histórico de agendamentos</EmptyText></EmptyList>}
          />
        </ContainerContent>
      </Container>
    </>
  )
}

export default ScheduleHistory;