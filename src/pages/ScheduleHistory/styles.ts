import styled from 'styled-components/native';
import { colors } from '../../styles/color';

export const Container = styled.View`
  flex: 1;

  padding: 20px;

  background: ${colors.primary};
`;

export const ContainerContent = styled.View`
  flex: 1;

  padding: 10px 0px;
`;

export const NextSchedules = styled.Text`
  font-family: 'RobotoSlab-Medium';
  font-size: 18px;
  margin-bottom: 10px;
  color: ${colors.white};
`;

export const EmptyList = styled.View`
  flex: 1;

  height: 200px;

  align-items: center;
  justify-content: center;
`;

export const EmptyText = styled.Text`
  font-family: 'RobotoSlab-Medium';
  font-size: 18px;
  color: ${colors.white};

  text-align: center;
`;

export const ContainerModal = styled.View`
  flex: 1;
  
  justify-content: center;
  align-items: center;
`;