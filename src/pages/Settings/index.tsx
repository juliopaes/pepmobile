import React from 'react';
import { Text } from 'react-native';

import { Container } from './styles';

export const Settings: React.FC = () => {
  return (
    <Container>
      <Text>Settings</Text>
    </Container>
  )
}

export default Settings;