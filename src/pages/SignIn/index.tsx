import React, { useCallback, useState, useRef } from 'react';
import {
  Platform,
  KeyboardAvoidingView,
  ScrollView,
  TextInput,
  ActivityIndicator,
  Modal,
} from 'react-native';
import { useNavigation } from '@react-navigation/core';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Form } from '@unform/mobile';
import { FormHandles } from '@unform/core';
import Input from '../../components/Input';

import logoAsq from '../../assets/images/logo.png';

import {
  Container,
  Title,
  TextForgotPassword,
  TextSignUp,
  TextSignUpUnderline,
  TextFooter,
  LogoFooter,
  ContainerModal,
} from './styles';
import { Button } from '../../components/Button';
import { colors } from '../../styles/color';
import { useAuth } from '../../hooks/auth';
import { useMessage } from '../../hooks/useMessage';
import * as Yup from 'yup';
import { getValidationErrors } from '../../utils/getValidationErrors';

interface ILoginProps {
  cpf: string;
  password: string;
}

export const SignIn: React.FC = () => {
  const [showModal, setShowModal] = useState(false);
  const formRef = useRef<FormHandles>(null);
  const passwordInputRef = useRef<TextInput>(null);
  const { navigate } = useNavigation();
  const { signIn } = useAuth();
  const { addToast } = useMessage();

  const handleSignIn = useCallback(async (data: ILoginProps) => {
    setShowModal(true);

    try {
      const schema = Yup.object().shape({
        cpf: Yup.string().length(14, "CPF inválido"),
        password: Yup.string().min(6, "A senha deve conter no mínimo 6 caracteres")
      });

      await schema.validate(data, {
        abortEarly: false,
      });

      const { cpf, password } = data;

      await signIn({ cpf, password });
    } catch (e) {
      setShowModal(false);

      if (e instanceof Yup.ValidationError) {
        const errors = getValidationErrors(e);

        formRef.current?.setErrors(errors);

        const key = Object.keys(errors)

        addToast({
          message: errors[key[0]],
          type: 'error'
        });

        return;
      }

      addToast({
        message: e.response?.data?.message || e.message,
        type: 'error'
      });
    }
  }, []);

  return (
    <>
      <Modal
        animationType="fade"
        visible={showModal}
        transparent={true}
      >
        <ContainerModal>
          <ActivityIndicator size="large" color="#FFF" />
        </ContainerModal>
      </Modal>
      <Container>
        <KeyboardAvoidingView
          style={{ flex: 1 }}
          behavior={Platform.OS === 'ios' ? 'padding' : undefined}
          enabled
        >
          <ScrollView
            keyboardShouldPersistTaps='handled'
            contentContainerStyle={{ flex: 1, justifyContent: 'center', opacity: showModal ? 0.5 : 1 }}
          >
            <Title>Nome do{'\n'}aplicativo aqui</Title>

            <Form ref={formRef} onSubmit={handleSignIn}>
              <Input
                name="cpf"
                icon="user"
                autoCorrect={false}
                autoCapitalize="none"
                keyboardType="numeric"
                text="CPF"
                type="cpf"
                returnKeyType="next"
                onSubmitEditing={() => {
                  passwordInputRef.current?.focus();
                }}
              />
              <Input
                ref={passwordInputRef}
                name="password"
                secureTextEntry={true}
                icon="lock"
                text="SENHA"
                returnKeyType="send"
                onSubmitEditing={() => {
                  formRef.current?.submitForm();
                }}
              />

              <TouchableOpacity onPress={() => { navigate('Forgot') }}>
                <TextForgotPassword>
                  Esqueci minha senha
                </TextForgotPassword>
              </TouchableOpacity>

              <Button
                onPress={() => {
                  formRef.current?.submitForm();
                }}
                color={colors.secondary}
              >
                Entrar
              </Button>

              <TouchableOpacity onPress={() => { navigate('SignUp') }}>
                <TextSignUp>
                  Ainda não definiu sua senha?{'\n'}
                  <TextSignUpUnderline>
                    Clique aqui
                  </TextSignUpUnderline> para definir
                </TextSignUp>
              </TouchableOpacity>
            </Form>

            <TextFooter>Desenvolvido por  <LogoFooter source={logoAsq} resizeMode="contain" /></TextFooter>
          </ScrollView>
        </KeyboardAvoidingView>
      </Container>
    </>
  )
}

export default SignIn;