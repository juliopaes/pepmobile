import styled from 'styled-components/native';
import { colors } from '../../styles/color';

export const Container = styled.View`
  flex: 1;
  background: ${colors.primary};

  padding: 0px 20px;

  justify-content: center;
`;

export const Logo = styled.Image`
  width: 80px;
  height: 80px;

  align-self: center;
`;

export const Title = styled.Text`
  font-size: 18px;
  font-family: 'RobotoSlab-Medium';

  color: ${colors.white};

  margin-bottom: 20px;
  text-align: center;
`;

export const TextForgotPassword = styled.Text`
  font-size: 13px;
  font-family: 'RobotoSlab-Regular';

  color: ${colors.white};

  text-decoration: underline;

  margin: 10px 0px;
`;

export const TextSignUp = styled.Text`
  font-size: 13px;
  font-family: 'RobotoSlab-Regular';

  color: ${colors.white};
  text-align: center;

  margin: 10px 0px;
`;

export const TextSignUpUnderline = styled.Text`
  font-size: 13px;
  font-family: 'RobotoSlab-Regular';

  color: ${colors.white};

  text-decoration: underline;
`;

export const LogoFooter = styled.Image`
  width: 50px;
  height: 50px;
`;

export const TextFooter = styled.Text`
  font-size: 13px;
  font-family: 'RobotoSlab-Medium';

  color: ${colors.white};
  text-align: center;
`;

export const ContainerModal = styled.View`
  flex: 1;
  
  justify-content: center;
  align-items: center;
`;