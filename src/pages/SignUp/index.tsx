import React, { useCallback, useRef, useState } from 'react';
import {
  Platform,
  KeyboardAvoidingView,
  ScrollView,
  TextInput,
  Modal,
  ActivityIndicator
} from 'react-native';
import { Dispatch } from 'redux';
import { useDispatch } from 'react-redux';
import { useNavigation } from '@react-navigation/native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { FormHandles } from '@unform/core';
import Input from '../../components/Input';
import { Button } from '../../components/Button';
import { colors } from '../../styles/color';
import Feather from 'react-native-vector-icons/Feather';
import { api } from '../../services/api'
import * as Yup from 'yup';
import { useMessage } from '../../hooks/useMessage';
import { addCpf } from '../../redux/actions/SignUp'
import { IDispatchProps } from '../../redux/types';

import { getValidationErrors } from '../../utils/getValidationErrors';

import logoAsq from '../../assets/images/logo.png';

import {
  Container,
  Logo,
  Title,
  StyledForm,
  ContainerStep,
  TextFooter,
  LogoFooter,
  ContainerModal,
} from './styles';

interface IFormProps {
  cpf: string;
  name: string;
  password: string;
  confirmPassword: string;
}

export const SignUp: React.FC = () => {
  const dispatch = useDispatch<Dispatch<IDispatchProps>>();
  const formRef = useRef<FormHandles>(null);
  const nameInputRef = useRef<TextInput>(null);
  const passwordInputRef = useRef<TextInput>(null);
  const confirmPasswordInputRef = useRef<TextInput>(null);
  const { goBack, navigate } = useNavigation();
  const [showModal, setShowModal] = useState(false);
  const { addToast } = useMessage();

  const handleCompleteSignUp = useCallback(async (data: IFormProps) => {
    setShowModal(true)

    try {
      const schema = Yup.object().shape({
        cpf: Yup.string().length(14, "CPF inválido"),
        name: Yup.string().required("Informe o nome"),
        password: Yup.string().required("Informe o password").min(6, "A senha deve conter no mínimo 6 caracteres"),
        confirmPassword: Yup.string().oneOf(
          [Yup.ref('password'), null],
          'As senhas não conferem',
        )
      });

      await schema.validate(data, {
        abortEarly: false,
      });

      await api.post('/user', {
        cpf: data.cpf,
        name: data.name,
        password: data.password
      });

      setShowModal(false)

      dispatch(addCpf(data.cpf));

      addToast({
        message: 'Cadastro efetuado com sucesso!',
        type: 'success'
      });

      navigate('SignIn');
    } catch (e) {
      setShowModal(false)

      if (e instanceof Yup.ValidationError) {
        const errors = getValidationErrors(e);

        formRef.current?.setErrors(errors);

        const key = Object.keys(errors)

        addToast({
          message: errors[key[0]],
          type: 'error'
        });

        return;
      }

      addToast({
        message: e.response?.data?.message || e.message,
        type: 'error'
      });
    }
  }, []);

  return (
    <>
      <Modal
        animationType="fade"
        visible={showModal}
        transparent={true}
      >
        <ContainerModal>
          <ActivityIndicator size="large" color="#FFF" />
        </ContainerModal>
      </Modal>
      <Container enableOpacity={showModal}>
        <TouchableOpacity onPress={() => goBack()}>
          <Feather name='chevron-left' size={25} color='#FFF' style={{ marginTop: 25 }} />
        </TouchableOpacity>
        <KeyboardAvoidingView
          style={{ flex: 1, }}
          behavior={Platform.OS === 'ios' ? 'padding' : undefined}
          enabled
        >
          <ScrollView
            keyboardShouldPersistTaps='handled'
            contentContainerStyle={{ flex: 1, opacity: showModal ? 0.5 : 1 }}
            showsVerticalScrollIndicator={false}
          >
            <ContainerStep>
              <Logo source={logoAsq} resizeMode="contain" />

              <Title>Insira seus dados{'\n'}para acessar o App</Title>

              <StyledForm
                ref={formRef}
                onSubmit={handleCompleteSignUp}
              >
                <Input
                  name="cpf"
                  icon="user"
                  autoCorrect={false}
                  autoCapitalize="none"
                  keyboardType="numeric"
                  text="CPF"
                  returnKeyType="next"
                  type='cpf'
                  onSubmitEditing={() => {
                    nameInputRef.current?.focus();
                  }}
                />

                <Input
                  ref={nameInputRef}
                  name="name"
                  icon="user"
                  autoCorrect={false}
                  autoCapitalize="words"
                  keyboardType="default"
                  text="NOME"
                  returnKeyType="next"
                  onSubmitEditing={() => {
                    passwordInputRef.current?.focus();
                  }}
                />

                <Input
                  ref={passwordInputRef}
                  name="password"
                  secureTextEntry={true}
                  icon="lock"
                  text="SENHA"
                  returnKeyType="next"
                  onSubmitEditing={() => {
                    confirmPasswordInputRef.current?.focus();
                  }}
                />

                <Input
                  ref={confirmPasswordInputRef}
                  name="confirmPassword"
                  secureTextEntry={true}
                  icon="lock"
                  text="CONFIRME A SENHA"
                  returnKeyType="send"
                  onSubmitEditing={() => {
                    formRef.current?.submitForm();
                  }}
                />

                <Button
                  onPress={() => {
                    formRef.current?.submitForm();
                  }}
                  color={colors.secondary}
                >
                  Cadastrar
                </Button>
                <TextFooter>Desenvolvido por  <LogoFooter source={logoAsq} resizeMode="contain" /></TextFooter>
              </StyledForm>
            </ContainerStep>
          </ScrollView>
        </KeyboardAvoidingView>
      </Container>
    </>
  )
}

export default SignUp;