import styled, { css } from 'styled-components/native';
import { Form } from '@unform/mobile';
import { colors } from '../../styles/color';

interface IContainerProps {
  enableOpacity?: boolean;
}

export const Container = styled.View<IContainerProps>`
  flex: 1;
  background: ${colors.primary};

  padding: 0px 20px;

  justify-content: center;

  ${props => props.enableOpacity &&
    css`
      opacity: 1;
    `
  }
`;

export const ContainerModal = styled.View`
  flex: 1;
  
  justify-content: center;
  align-items: center;
`;

export const Logo = styled.Image`
  width: 100px;
  height: 100px;

  margin: 15px 0px;
`;

export const Title = styled.Text`
  font-size: 18px;
  font-family: 'RobotoSlab-Medium';

  color: ${colors.white};

  margin-top: 10px;
`;

export const StyledForm = styled(Form)`
  margin: 30px 0px;
`;

export const ContainerStep = styled.ScrollView``;

export const TextFooter = styled.Text`
  font-size: 13px;
  font-family: 'RobotoSlab-Medium';

  color: ${colors.white};
  text-align: center;
`;

export const LogoFooter = styled.Image`
  width: 50px;
  height: 50px;
`;
