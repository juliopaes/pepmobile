import * as type from '../../reducers/Schedule/types';

interface ISelectSchedule {
  doctorId: number;
  hour: string;
  hourF: string;
}

export function selectDoctor(selectedDoctor: string) {
  return {
    type: type.SCHEDULE_DOCTOR,
    payload: {
      doctorId: selectedDoctor
    }
  }
};

export function selectType(selectedType: number) {
  return {
    type: type.SCHEDULE_TYPE,
    payload: {
      type: selectedType
    }
  }
};

export function selectDate(selectedDate: string) {
  return {
    type: type.SCHEDULE_DATE,
    payload: {
      date: selectedDate
    }
  }
};

export function selectHour(selectedHour: string) {
  return {
    type: type.SCHEDULE_HOUR,
    payload: {
      hour: selectedHour
    }
  }
};

export function clearSchedule() {
  return {
    type: type.CLEAR_SCHEDULE,
    payload: null
  }
}

export function selectSchedule({ doctorId, hour, hourF }: ISelectSchedule) {
  return {
    type: type.SCHEDULE_SELECTED,
    payload: {
      doctorId,
      hour,
      hourF
    }
  }
}