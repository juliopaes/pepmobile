import * as type from '../../types';

export function addCpf(cpf: string) {
  return {
    type: type.ADD_CPF,
    payload: cpf
  }
};