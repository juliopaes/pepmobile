import * as type from './types';

const initialState = {
  doctorId: null,
  hour: null,
  hourF: null,
  date: null,
  type: null,
};

interface IActionProps {
  type: string;
  payload: any;
}

export default function schedule(state = initialState, action: IActionProps) {
  switch (action.type) {
    case type.SCHEDULE_DOCTOR:
      return {
        ...state,
        doctorId: action.payload.doctorId
      };
    case type.SCHEDULE_DATE:
      return {
        ...state,
        date: action.payload.date
      };
    case type.SCHEDULE_HOUR:
      return {
        ...state,
        hour: action.payload.hour
      };
    case type.SCHEDULE_TYPE:
      return {
        ...state,
        type: action.payload.type
      }
    case type.SCHEDULE_SELECTED:
      return {
        ...state,
        hourF: action.payload.hourF,
        hour: action.payload.hour,
        doctorId: action.payload.doctorId
      }
    case type.CLEAR_SCHEDULE:
      return {
        ...initialState
      }
    default:
      return state;
  }
}