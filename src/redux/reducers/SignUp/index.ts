import * as type from '../../types';

const initialState = {
  cpf: null
};

interface IActionProps {
  type: string;
  payload: any;
}

export default function signUp(state = initialState, action: IActionProps) {
  switch (action.type) {
    case type.ADD_CPF:
      return {
        ...state,
        cpf: action.payload
      };
    default:
      return state;
  }
}