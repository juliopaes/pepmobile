import { combineReducers } from 'redux';
import SignUp from './SignUp';
import Schedule from './Schedule';

const rootReducer = combineReducers({
  signUp: SignUp,
  schedule: Schedule
});

export default rootReducer;