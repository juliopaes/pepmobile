export type IDispatchProps = {
  type: string;
  payload: any
}

export const ADD_CPF = 'ADD_CPF';