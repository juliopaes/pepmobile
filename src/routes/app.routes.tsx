import React from 'react';
import { NavigationContainer, NavigationContainerRef } from '@react-navigation/native';
import { createDrawerNavigator } from '@react-navigation/drawer';

import DrawerContent from '../pages/DrawerContent';

import Home from '../pages/Home';
import Schedule from '../pages/Schedule';
import Exams from '../pages/Exams';
import Goals from '../pages/Goals';
import Settings from '../pages/Settings';
import CreateSchedule from '../pages/CreateSchedule';
import ConfirmSchedule from '../pages/ConfirmSchedule';
import ScheduleHistory from '../pages/ScheduleHistory';
import Logout from '../pages/Logout';

const { Navigator: NavigatorDrawer, Screen: ScreenDrawer } = createDrawerNavigator();

const navigationRef = React.createRef<NavigationContainerRef>();

export const navigate = (name: string, params?: any) => {
	if (navigationRef.current) {
		navigationRef.current.navigate(name, params);
	}
};

const AppStack: React.FC = () => {
	return (
		<NavigationContainer ref={navigationRef}>
			<NavigatorDrawer screenOptions={{ headerShown: false }} drawerContent={(props) => <DrawerContent {...props} />} >
				<ScreenDrawer name="Home" component={Home} />
				<ScreenDrawer name="Schedule" component={Schedule} />
				<ScreenDrawer name="Exams" component={Exams} />
				<ScreenDrawer name="Goals" component={Goals} />
				<ScreenDrawer name="Settings" component={Settings} />
				<ScreenDrawer name="CreateSchedule" component={CreateSchedule} />
				<ScreenDrawer name="ConfirmSchedule" component={ConfirmSchedule} />
				<ScreenDrawer name="ScheduleHistory" component={ScheduleHistory} />
				<ScreenDrawer name="Logout" component={Logout} />
			</NavigatorDrawer>
		</NavigationContainer>
	)
};

export default AppStack;