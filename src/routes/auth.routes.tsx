import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import SignIn from '../pages/SignIn';
import SignUp from '../pages/SignUp';
import Forgot from '../pages/Forgot';

const { Navigator, Screen } = createStackNavigator();

const AppStack: React.FC = () => {
	return (
		<NavigationContainer>
			<Navigator screenOptions={{ headerShown: false }} >
				<Screen name="SignIn" component={SignIn} />
				<Screen name="SignUp" component={SignUp} />
				<Screen name="Forgot" component={Forgot} />
			</Navigator>
		</NavigationContainer>
	)
};

export default AppStack;