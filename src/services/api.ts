import { navigate } from '../routes/app.routes'
import axios from 'axios';

const responseError = async (error: any) => {
  if (error.response && [401].includes(error.response.status)) {
    navigate('Logout');
  }

  return Promise.reject(error);
}

export const api = axios.create({
  baseURL: 'http://192.168.0.103:3333'
});

api.interceptors.response.use(undefined, responseError);