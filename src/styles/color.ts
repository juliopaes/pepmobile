export const colors = {
  primary: '#155a7e',
  secondary: '#3FA7DE',
  orange: '#ee3625',
  white: '#FFF',
  gray: '#7A7A7A',
  grayLight: '#c2c2c2',
  black: '#43425D',
  red: '#d1381a',
  messages: {
    error: '#c53030',
    success: '#2e656a',
    info: '#3172b7',
    background: {
      error: '#fddede',
      success: '#e6fffa',
      info: '#ebf8ff',
    }
  },
  error: {
    primary: '#c53030'
  }
}